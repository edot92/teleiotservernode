require('dotenv').config()
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'starter',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        type: 'text/css',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
      },
    ],
    script: [
      {
        src: 'https://unpkg.com/babel-polyfill/dist/polyfill.min.js',
      },
      {
        src: 'https://cdn.jsdelivr.net/npm/sweetalert',
      },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
      },
      {
        src: 'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js',
      },
      {
        src:
          'https://cdn.rawgit.com/Mikhus/canvas-gauges/gh-pages/download/2.1.4/all/gauge.min.js',
      },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.4/echarts.min.js',
      },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment-with-locales.min.js',
      },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js',
      },
      {
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js',
      },
    ],
  },
  /*
  ** Global CSS
  */
  css: ['~/assets/css/main.css', '~/assets/css/app.styl'],
  modules: ['@nuxtjs/axios'],
  axios: {
    // baseURL: environment.API_URL,
    // requestInterceptor: (config, { store }) => {
    //   config.headers.common['Authorization'] = 'Bearer ' + store.state.token
    //   return config
    // },
  },
  /*
  ** Add axios globally
  */
  build: {
    vendor: ['axios', 'vuetify'],
    extractCSS: true,
    /*
    ** Run ESLINT on save
    */
    extend(config, ctx) {
      if (ctx.isClient) {
        config.module.rules.push(
          {
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/,
          },
          // {
          //   test: /\.js$/,
          //   loader: 'babel-loader',
          //   exclude: /node_modules/
          // },
          {
            test: /\.(png|jpg|gif|svg)$/,
            loader: 'url-loader',
            options: {
              name: '[name].[ext]?[hash]',
            },
          },
        )
      }
    },
  },
  /*
  ** Load Vuetify into the app
  */
  plugins: [
    '~/plugins/vuetify',
    '~/plugins/moment',
    { src: '~/plugins/localStorage.js', ssr: false },
  ],
  router: {
    middleware: ['firstinit'],
  },
  /*
  ** Load Vuetify CSS globally
  */
  // css: []
}
