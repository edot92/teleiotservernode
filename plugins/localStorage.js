// ~/plugins/localStorage.js

import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR }) => {
  if (isHMR) return
  window.onNuxtReady(() => {
    createPersistedState({
      key: 'vuex',
      paths: [],
    })(store)
  })
}
