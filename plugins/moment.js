import Vue from 'vue'
// require('moment/locale/id')
import VueClipboard from 'vue-clipboard2'

// var moment = require('moment')
// Vue.use(moment)

// Vue.use(require('vue-moment'))
const moment = require('moment')
require('moment/locale/id')
Vue.use(require('vue-moment'), {
  moment,
})

Vue.use(VueClipboard)
