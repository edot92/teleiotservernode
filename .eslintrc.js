module.exports = {
  root: true,
  parser: 'babel-eslint',
  env: {
    browser: true,
    node: true,
  },
  extends: 'standard',
  // required to lint *.vue files
  plugins: ['html'],
  // add your custom rules here
  rules: {
    // 'space-before-function-paren': 0,
    // skipBlankLines: true,
    // ignoreComments: true,
    'comma-dangle': 0, //0 = off, 1 = warn, 2 = error
  },
  globals: {
    RadialGauge: true,
    Cookies: true,
    hello: true,
    panzoom: true,
    Chartist: true,
    io: true,
    Chart: true,
    moment: true,
    axios: true,
    svgson: true,
    VuexFire: true,
    firebase: true,
    window: true,
    $: true,
    SVG: true,
  },
}
