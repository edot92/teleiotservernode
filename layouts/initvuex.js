export default {
  initVuex (thisV) {
    // cek ls
    return new Promise(function (resolve, reject) {
      let temp = window.localStorage.getItem('vuex')
      if (temp) {
        try {
          // get User ls and set User
          let ls = JSON.parse(temp)
          thisV.$store.commit('User/init', ls.User)
          thisV.$store.commit('setBaseUrl', window.location.hostname)
          resolve(ls.User)
        } catch (error) {
          thisV.$store.commit('setBaseUrl', window.location.hostname)
          alert(error)
          console.error(error)
          reject(error)
        }
      } else {
        let eerr = 'not found last session'
        thisV.$store.commit('setBaseUrl', window.location.hostname)
        reject(eerr)
      }
    })
  },
}
