import Vuex from 'vuex'
import User from './user'
import Device from './device'
import '../plugins/localStorage'
// import createPersistedState from 'vuex-persistedstate'
// import * as Cookies from 'js-cookie'

// import createPersistedState from 'vuex-persistedstate'
// import * as Cookies from 'js-cookie'

const createStore = () => {
  return new Vuex.Store({
    state: {
      portRest: ':8080',
      urlRest: process.env.URLREST || 'http://localhost:8080/api',
      urlWS: process.env.URLWS || 'http://localhost:8080',
      loading: true,
      // authUser: {
      //   kikuk: 'sasasas'
      // }
    },
    mutations: {
      setBaseUrl(state, data) {
        if ( String( data ).indexOf( "heroku" ) >= 0 ) {
        if (process.env.NODE_ENV === 'development') {
          if (!!process.env.URLREST === false) {
            var protokol = window.location.protocol + '//'
            state.urlRest = protokol + data + state.portRest + '/api'
            state.urlWS = protokol + data + state.portRest
          }
        }}
      },
      setLoading(state, data) {
        state.loading = data
        $(document).trigger('setLoading', data)
      },
      // SET_USER: function (state, user) {
      //   state.authUser = user
      // }
    },
    actions: {
      // nuxtServerInit ({ commit }, { req }) {
      //   if (req.session && req.user) {
      //     commit('SET_USER', req.user)
      //   }
      // }
      async nuxtServerInit({ commit, dispatch }, { req, res }) {},
    },
    // plugins: [
    //   createPersistedState({
    //     storage: {
    //       getItem: key => Cookies.get(key),
    //       // Please see https://github.com/js-cookie/js-cookie#json, on how to handle JSON.
    //       setItem: (key, value) =>
    //         Cookies.set(key, value, { expires: 3, secure: false }),
    //       removeItem: key => Cookies.remove(key),
    //     },
    //   }),
    // ],
    // plugins: process.browser
    //   ? [
    //       createPersistedState({
    //         storage: window.sessionStorage,
    //       }),
    //     ]
    //   : [],
    modules: {
      User,
      Device,
    },
  })
}

export default createStore
