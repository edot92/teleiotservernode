import axios from '../plugins/axios'

export default {
  namespaced: true,
  state: {
    budijwt: '',
    role: '',
  },
  mutations: {
    init(state, data) {
      state.budijwt = data.budijwt
      state.role = data.role
    },
    setJwt(state, data) {
      state.budijwt = data
    },
    setUser(state, data) {
      state.role = data.role
      state.budijwt = data.jwt
    },
  },
  actions: {
    setJwt({ commit }, { user, jwt }) {
      return new Promise(function(resolve, reject) {
        commit('setJwt', { user, jwt })
        resolve()
      })
    },
    setUser({ commit }, data) {
      return new Promise(function(resolve, reject) {
        commit('setUser', data)
        resolve(true)
      })
    },
    isAuth() {
      const thisV = this
      return new Promise(function(resolve, reject) {
        if (!!thisV.state.User.budijwt) {
          resolve(true)
        } else {
          resolve(false)
        }
      })
    },
    httpLogin({ commit }, { username, password }) {
      return new Promise(function(resolve, reject) {
        axios({
          method: 'get',
          url: '/api/user/login/' + username + '/' + password,
        })
          .then(value => {
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            reject(err)
          })
      })
    },
    /** ******** petugas ****************/
    // httpPetugasGetList({ commit }, { offset, limit }) {
    //   let jwt = this.state.User.budijwt
    //   const thisV = this
    //   return new Promise(function(resolve, reject) {
    //     if (jwt === undefined || jwt === '' || !!jwt === false) {
    //       var error = 'jwt not vaild'
    //       reject(error)
    //     } else {
    //       axios({
    //         method: 'get',
    //         url:
    //           '/api/user/petugas/getlist/' +
    //           offset +
    //           '/' +
    //           limit +
    //           '/' +
    //           thisV.state.User.budijwt +
    //           '/',
    //       })
    //         .then(value => {
    //           resolve(value.data)
    //         })
    //         .catch(err => {
    //           reject(err)
    //         })
    //     }
    //   })
    // },
    httpPetugasAddNew({ commit }, data) {
      let jwt = this.state.User.budijwt
      const thisV = this
      return new Promise(function(resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          axios({
            method: 'post',
            url: '/api/user/petugas/addnew/' + thisV.state.User.budijwt,
            data: data,
          })
            .then(value => {
              resolve(value.data)
            })
            .catch(err => {
              reject(err)
            })
        }
      })
    },
    /** ********pelanggan ****************/
    httpPelangganGetList({ commit }, { offset, limit }) {
      let jwt = this.state.User.budijwt
      const thisV = this
      return new Promise(function(resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          axios({
            method: 'get',
            url:
              '/api/user/pelanggan/getlist/' +
              offset +
              '/' +
              limit +
              '/' +
              thisV.state.User.budijwt +
              '/',
          })
            .then(value => {
              resolve(value.data)
            })
            .catch(err => {
              reject(err)
            })
        }
      })
    },
  },
}
