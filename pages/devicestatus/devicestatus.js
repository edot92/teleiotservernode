// tidak pakai vuex
import axios from '~/plugins/axios.js'

export default {
  actions: {
    GetAllDeviceByEmailuser ({ thisV }) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'get',
            url:
              thisV.$store.state.urlRest +
              '/device/auth/GetAllDeviceByEmailuser/' +
              '/' +
              jwt,
            headers: {
              budijwt: jwt,
            },
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
  },
}
