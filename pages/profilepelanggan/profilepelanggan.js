export default {
  actions: {
    HttpUpdateProfilePelanggan (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        thisV.$store.commit('setLoading', true)
        thisV
          .$axios({
            method: 'post',
            url: thisV.$store.state.urlRest + '/pelanggan/auth/Updateprofile',
            data: data,
            headers: {
              budijwt: jwt,
            },
          })
          .then(value => {
            thisV.$store.commit('setLoading', false)
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            thisV.$store.commit('setLoading', false)
            reject(err)
          })
      })
    },
    HttpGetProfilePelanggan (thisV) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        thisV.$store.commit('setLoading', true)
        thisV
          .$axios({
            method: 'get',
            url:
              thisV.$store.state.urlRest + '/pelanggan/auth/GetProfile/' + jwt,
            headers: {
              budijwt: jwt,
            },
          })
          .then(value => {
            thisV.$store.commit('setLoading', false)
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            thisV.$store.commit('setLoading', false)
            reject(err)
          })
      })
    },
    HttpTestSendTelergram (thisV, namaLengkap, idTelegram) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        thisV.$store.commit('setLoading', true)
        thisV
          .$axios({
            method: 'get',
            url:
              thisV.$store.state.urlRest +
              '/pelanggan/auth/sendtelegram/' +
              namaLengkap +
              '/' +
              idTelegram +
              '/' +
              jwt,
            headers: {
              budijwt: jwt,
            },
          })
          .then(value => {
            thisV.$store.commit('setLoading', false)
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            thisV.$store.commit('setLoading', false)
            reject(err)
          })
      })
    },
  },
}
