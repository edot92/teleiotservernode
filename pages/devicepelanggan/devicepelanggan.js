// tidak pakai vuex

export default {
  actions: {
    httpDevicePelangganEdit (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          thisV
            .$axios({
              method: 'post',
              url:
                thisV.$store.state.urlRest +
                '/device/auth/UpdateDeviceByPelanggan/' +
                jwt,
              data: data,
              headers: {
                budijwt: jwt,
              },
            })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
    httpBuatDeviceBaruPelanggan (thisV) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          thisV
            .$axios({
              method: 'get',
              url:
                thisV.$store.state.urlRest +
                '/device/auth/BuatDeviceBaruPelanggan/' +
                jwt,
              headers: {
                budijwt: jwt,
              },
            })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
    httpDeviceaddToPelanggan (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          thisV
            .$axios({
              method: 'post',
              url:
                thisV.$store.state.urlRest +
                '/device/auth/DeviceaddToPelanggan/' +
                jwt,
              data: data,
              headers: {
                budijwt: jwt,
              },
            })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
    httpGetDeviceListPelanggan ({ thisV, offset, limit }) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          thisV
            .$axios({
              method: 'get',
              url:
                thisV.$store.state.urlRest +
                '/device/auth/GetDeviceListPelangganPagination/' +
                offset +
                '/' +
                limit +
                '/' +
                jwt,
              headers: {
                budijwt: jwt,
              },
            })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
  },
}
