import axios from '~/plugins/axios'
export default {
  actions: {
    HttpGetProfilePetugas (thisV) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        thisV.$store.commit('setLoading', true)
        axios({
          method: 'get',
          url: thisV.$store.state.urlRest + '/petugas/auth/GetProfile/' + jwt,
          headers: {
            budijwt: jwt,
          },
        })
          .then(value => {
            thisV.$store.commit('setLoading', false)
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            thisV.$store.commit('setLoading', false)
            reject(err)
          })
      })
    },
  },
}
