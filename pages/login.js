export default {
  actions: {
    HttpLoginPetugas (thisV, username, password) {
      return new Promise(function (resolve, reject) {
        thisV.$store.commit('setLoading', true)
        thisV
          .$axios({
            method: 'get',
            url:
              thisV.$store.state.urlRest +
              '/petugas/login/' +
              username +
              '/' +
              password,
          })
          .then(value => {
            thisV.$store.commit('setLoading', false)
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            thisV.$store.commit('setLoading', false)
            reject(err)
          })
      })
    },
    HttpLoginPelanggan (thisV, username, password) {
      return new Promise(function (resolve, reject) {
        thisV.$store.commit('setLoading', true)
        thisV
          .$axios({
            method: 'get',
            url:
              thisV.$store.state.urlRest +
              '/pelanggan/login/' +
              username +
              '/' +
              password,
          })
          .then(value => {
            thisV.$store.commit('setLoading', false)
            // console.log(value)
            resolve(value.data)
          })
          .catch(err => {
            thisV.$store.commit('setLoading', false)
            reject(err)
          })
      })
    },
  },
}
