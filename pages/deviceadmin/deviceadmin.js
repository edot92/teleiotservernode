import axios from '~/plugins/axios.js'
export default {
  actions: {
    httpDevicePelangganDelete (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          thisV
            .$axios({
              method: 'post',
              url:
                thisV.$store.state.urlRest +
                '/device/auth/DeleteDeviceByPetugas/' +
                jwt,
              data: data,
              headers: {
                budijwt: jwt,
              },
            })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },

    httpDevicePelangganEdit (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          thisV
            .$axios({
              method: 'post',
              url:
                thisV.$store.state.urlRest +
                '/device/auth/UpdateDeviceByPetugas/' +
                jwt,
              data: data,
              headers: {
                budijwt: jwt,
              },
            })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },

    /** ********  ****************/
    httpGeneratedDevice (thisV) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'get',
            headers: {
              budijwt: jwt,
            },
            url: thisV.$store.state.urlRest + '/device/auth/addnew/' + jwt,
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
    httpDeviceGetList ({ thisV, offset, limit }) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'get',
            headers: {
              budijwt: jwt,
            },
            url:
              thisV.$store.state.urlRest +
              '/device/auth/GetlistPagination/' +
              offset +
              '/' +
              limit +
              '/' +
              jwt,
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
  },
}
