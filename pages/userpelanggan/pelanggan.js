// tidak pakai vuex
import axios from '~/plugins/axios.js'

export default {
  actions: {
    httpPelangganAddNew (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'post',
            url: thisV.$store.state.urlRest + '/pelanggan/addnew/' + jwt,
            data: data,
            headers: {
              budijwt: jwt,
            },
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
    httpPelangganGetList ({ thisV, offset, limit }) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'get',
            headers: {
              budijwt: jwt,
            },
            url:
              thisV.$store.state.urlRest +
              '/pelanggan/auth/GetlistPagination/' +
              offset +
              '/' +
              limit +
              '/' +
              jwt,
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
  },
}
