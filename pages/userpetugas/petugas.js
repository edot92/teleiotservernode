import axios from '~/plugins/axios.js'
export default {
  actions: {
    /** ******** petugas ****************/
    httpPetugasGetList ({ thisV, offset, limit }) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'get',
            headers: {
              budijwt: jwt,
            },
            url:
              thisV.$store.state.urlRest +
              '/petugas/auth/GetlistPagination/' +
              offset +
              '/' +
              limit +
              '/' +
              jwt,
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
    httpPetugasAddNew (thisV, data) {
      var jwt = thisV.$store.state.User.budijwt
      return new Promise(function (resolve, reject) {
        if (jwt === undefined || jwt === '' || !!jwt === false) {
          var error = 'jwt not vaild'
          reject(error)
        } else {
          thisV.$store.commit('setLoading', true)
          axios({
            method: 'post',
            headers: {
              budijwt: jwt,
            },
            url: thisV.$store.state.urlRest + '/petugas/auth/addnew/' + jwt,
            data: data,
          })
            .then(value => {
              thisV.$store.commit('setLoading', false)
              resolve(value.data)
            })
            .catch(err => {
              thisV.$store.commit('setLoading', false)
              reject(err)
            })
        }
      })
    },
  },
}
